describe('My List page', () => {
  describe('with existing activities', () => {
    const initialActivities = [
      { key: 1, activity: 'Activity 1' },
      { key: 2, activity: 'Activity 2' },
      { key: 3, activity: 'Activity 3' },
    ]
    beforeEach(() => {
      localStorage.setItem('boredActivities', JSON.stringify(initialActivities))
      cy.visit('/my-list')
    })

    it('renders list from the cache', () => {
      initialActivities.forEach(activity => {
        cy.contains('.item', activity.activity)
      })
    })

    it('removes single activity', () => {
      cy.get('.item:last-child .js-remove').click()

      const runAssertions = () => {
        initialActivities.slice(0, -1).forEach(activity => {
          cy.contains('.item', activity.activity)
        })

        cy.get('.items-list').then(list => {
          expect(list.children().length).eq(initialActivities.length)
        })
      }

      runAssertions()
      cy.reload(true)
      runAssertions()
    })

    it('remove all activities', () => {
      cy.contains('button', 'Clear all').click()

      const runAssertions = () => {
        cy.get('.items-list').should('not.exist')
        cy.get('button.js-remove-all').should('not.exist')
        cy.contains('No activities saved.')
        cy.contains('a', 'Search here')
      }

      runAssertions()
      cy.reload(true)
      runAssertions()
    })
  })

  describe('when no existing activities', () => {
    it('shows message with link to search', () => {
      cy.contains('No activities saved.')
      cy.contains('a', 'Search here').click()
      cy.location('pathname').should('eq', '/')
    })
  })
})
