// https://docs.cypress.io/api/introduction/api.html

describe('Home page', () => {
  const fakeActivity = {
    activity: 'Fake activity',
    type: 'social',
    participants: 1,
    price: 0.1,
    key: 1234,
  }
  const apiUrl = 'http://www.boredapi.com/api/activity/'

  it('it renders activity view', () => {
    cy.visit('/')
    cy.contains('a', 'Activities')
    cy.contains('a', 'My list')
    cy.get('.activity')
      .get('.description')
      .get('.type')
      .get('.participants-count')
      .get('.budget')

    cy.contains('button.js-save', 'Save for later')
    cy.contains('button.js-search', 'Hit me with a new one')
  })

  it('fetches random act', () => {
    cy.server()
    cy.route({ url: apiUrl, response: fakeActivity }).as('fetchRandom')

    cy.visit('/')
    cy.wait('@fetchRandom')
    cy.contains('.activity .description', fakeActivity.activity)
    cy.get('.activity .type select').should('have.value', fakeActivity.type)
    cy.get('.activity .participants-count input').should(
      'have.value',
      fakeActivity.participants.toString()
    )
    cy.get('.activity .budget input').should(
      'have.value',
      fakeActivity.price.toString()
    )
  })

  it('shows error when fetch fails', () => {
    cy.server()
    const error = 'Fake error'
    cy.route({ url: apiUrl, response: { error } }).as('fetchRandom')

    cy.visit('/')
    cy.wait('@fetchRandom')

    cy.contains('.alert-danger', error)
    cy.get('.activity .description').should('not.exist')
  })

  it('searches for new activity', () => {
    cy.server()
    cy.route({
      url: apiUrl,
      response: fakeActivity,
    }).as('fetchRandom')
    cy.route({ url: apiUrl + '*' }).as('fetchActivity') // reset stub

    cy.visit('/')
    cy.wait('@fetchRandom')

    cy.get('.activity .type select').select('cooking')
    cy.get('.activity .participants-count input').type('{selectAll}2')
    cy.get('.activity .budget input')
      .invoke('val', '0.3')
      .trigger('change')

    cy.get('button.js-search').click()
    cy.wait('@fetchActivity')
      .its('url')
      .should('include', 'type=cooking')
      .should('include', 'participants=2')
      .should('include', 'minprice=0.01')
      .should('include', 'maxprice=0.35')

    cy.get('.activity .description').then($activity => {
      const updatedActivity = $activity.text()
      expect(updatedActivity).not.to.eq(fakeActivity.activity)
    })
  })

  it('saves selected activity', () => {
    cy.server()
    cy.route({
      url: apiUrl,
      response: fakeActivity,
    }).as('fetchRandom')
    cy.route({ url: apiUrl + '*' }).as('fetchActivity') // reset stub

    cy.visit('/')
    cy.wait('@fetchRandom')
    cy.get('button.js-save').click()

    cy.get('.activity .type select').select('cooking')
    cy.get('.activity .participants-count input').type('{selectAll}2')
    cy.get('.activity .budget input')
      .invoke('val', '0.3')
      .trigger('change')
    cy.get('button.js-search').click()

    cy.wait('@fetchActivity').then(xhr => {
      const fetchedActivity = xhr.response.body
      cy.get('button.js-save').click()

      cy.contains('a', 'My list').click()

      cy.url().should('include', '/my-list')

      const runAssertions = () => {
        cy.contains('.activities .items-list', fakeActivity.activity)
        cy.contains('.activities .items-list', fetchedActivity.activity)
      }

      runAssertions()
      cy.reload(true)
      runAssertions()
    })
  })
})
