# Bored App

Bored app is a client-side single page application for people who is bored.

It suggests the activities from [Bored API](http://www.boredapi.com/documentation) and allows saving them in the browser storage.

Live demo is available [here](https://boredapp.vbelolapotkov.com).

## App description

- When the user opens the **Activity** page (home page) a random activity is suggested.
- Pressing the **Hit me with a new one!** button will recommend a new activity according to the selected acivity details.
- Participants field allows entering numbers above 1 only.
- Error message is shown when there is no suggestions matching selected details or any other api errors. Previous activity if available is not removed in case of error.
- If suggestion is already in the saved list the app tries to refetch suggested activity 3 times.
- After pressing the **Save for later** button the current activity is stored. It will be available in the **My List page** as a list view.
- If the user closes the browser the app remembers the saved activities and they are available in the **My list** page.
- **My list page**: Here the user can see the saved activities. At the end of every row there is a button to delete the activity.
- A **Clear all** will delete all the saved activities.

## Tech stack

- UI Engine: [Vue](https://vuejs.org)
- Routing: [Vue router](https://router.vuejs.org/)
- UI Components & styling: [Bootstrap](https://bootstrap-vue.js.org/), [SASS](https://sass-lang.com/)
- HTTP: [axios](https://github.com/axios/axios)
- Icons: [Font Awesome](https://fontawesome.com/how-to-use/on-the-web/using-with/vuejs)
- Tests: [Cypress](https://www.cypress.io/)
- Hosting: [Netlify](https://netlify.com)

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Run your end-to-end tests

```
yarn test:e2e
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
