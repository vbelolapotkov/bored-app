import axios from 'axios'
const API_URL = 'https://www.boredapi.com/api/activity/'

export default async function fetchActivity({
  type,
  participants,
  price,
} = {}) {
  try {
    const queryParams = {
      ...(type && { type }),
      ...(participants !== undefined && { participants }),
      ...(price !== undefined && { minprice: price[0], maxprice: price[1] }),
    }

    const { data } = await axios.get(API_URL, { params: queryParams })
    if (data.error) {
      throw new Error(data.error)
    }
    return data
  } catch (err) {
    throw err
  }
}
