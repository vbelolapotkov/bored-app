// fake storage
const storage = storageAvailable('localStorage')
  ? window.localStorage
  : fakeLocalStorage()

export class Activities {
  constructor() {
    this._activities = new Map()
    this._restoreActivities()
  }

  add(activity) {
    this._activities.set(activity.key, activity)
    this._syncStorage()
  }

  getAll() {
    return [...this._activities.values()]
  }

  remove(activityKey) {
    this._activities.delete(activityKey)
    this._syncStorage()
  }

  clear() {
    this._activities.clear()
    this._syncStorage()
  }

  has(key) {
    return this._activities.has(key)
  }

  _restoreActivities() {
    try {
      const storedActivities = JSON.parse(storage.getItem('boredActivities'))
      if (!storedActivities) {
        return
      }

      storedActivities.forEach(activity =>
        this._activities.set(activity.key, activity)
      )
    } catch {
      return
    }
  }

  _syncStorage() {
    storage.setItem('boredActivities', JSON.stringify(this.getAll()))
  }
}

function fakeLocalStorage() {
  const storage = new Map()

  return Object.freeze({
    getItem,
    setItem,
    removeItem,
  })

  function setItem(key, value) {
    storage.set(key, value)
  }

  function getItem(key) {
    return storage.get(key)
  }

  function removeItem(key) {
    storage.delete(key)
  }
}

function storageAvailable(type) {
  var storage
  try {
    storage = window[type]
    var x = '__storage_test__'
    storage.setItem(x, x)
    storage.removeItem(x)
    return true
  } catch (e) {
    return (
      e instanceof DOMException &&
      // everything except Firefox
      (e.code === 22 ||
        // Firefox
        e.code === 1014 ||
        // test name field too, because code might not be present
        // everything except Firefox
        e.name === 'QuotaExceededError' ||
        // Firefox
        e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
      // acknowledge QuotaExceededError only if there's something already stored
      (storage && storage.length !== 0)
    )
  }
}
