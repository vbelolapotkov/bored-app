import { storiesOf } from '@storybook/vue'

import NumberInput from './NumberInput.vue'

storiesOf('NumberInput', module)
  .addDecorator(() => ({
    template: '<div style="padding: 20px"><story/></div>',
  }))
  .add('default', () => ({
    components: { NumberInput },
    template: `<number-input label="Number (1..10)" :value="10" :min="1" :max="10" />`,
  }))
  .add('disabled', () => ({
    components: { NumberInput },
    template: `<number-input label="Label" disabled/>`,
  }))
  .add('withoutLabel', () => ({
    components: { NumberInput },
    template: `<number-input />`,
  }))
