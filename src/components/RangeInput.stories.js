import { storiesOf } from '@storybook/vue'

import RangeInput from './RangeInput.vue'

storiesOf('RangeInput', module)
  .addDecorator(() => ({
    template: '<div style="padding: 20px"><story/></div>',
  }))
  .add('default', () => ({
    components: { RangeInput },
    template: `<range-input label="Range (0..1)" />`,
  }))
  .add('disabled', () => ({
    components: { RangeInput },
    template: `<range-input label="Label" disabled/>`,
  }))
  .add('customRange', () => ({
    components: { RangeInput },
    template: `<range-input label="From 0 to 10 with 4 steps" min="0" max="10" step="2.5"/>`,
  }))
  .add('customRangeLabels', () => ({
    components: { RangeInput },
    template: `<range-input label="From 0 to 10 with 4 steps" min="0" max="10" step="2.5" minLabel="Zero"  maxLabel="Ten"/>`,
  }))
