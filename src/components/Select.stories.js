import { storiesOf } from '@storybook/vue'

import FormSelect from './FormSelect.vue'

const options = [
  { value: '1', text: 'Option 1' },
  { value: '2', text: 'Option 2' },
  { value: '3', text: 'Option 3' },
  { value: '4', text: 'Option 4' },
  { value: '5', text: 'Option 5' },
]

storiesOf('FormSelect', module)
  .addDecorator(() => ({
    template: '<div style="padding: 20px"><story/></div>',
  }))
  .add('default', () => ({
    components: { FormSelect },
    template: `<form-select label="Select option" :options="options" />`,
    data: () => ({ options: [...options] }),
  }))
  .add('disabled', () => ({
    components: { FormSelect },
    template: `<form-select label="Select option" :options="options" disabled/>`,
    data: () => ({ options: [...options] }),
  }))
  .add('disabledOption', () => ({
    components: { FormSelect },
    template: `<form-select label="Select option" :options="options"/>`,
    data: () => ({
      options: [
        { value: null, text: 'Disabled option', disabled: true },
        ...options,
      ],
    }),
  }))
  .add('customRangeLabels', () => ({
    components: { FormSelect },
    template: `<form-select :options="options" :value="value"/>`,
    data: () => ({
      options: [...options],
      value: options[0].value,
    }),
  }))
