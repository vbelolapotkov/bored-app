import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'

import SecondaryButton from './SecondaryButton.vue'

const methods = {
  onClick: action('onButtonClick'),
}

storiesOf('SecondaryButton', module)
  .addDecorator(() => ({
    template: '<div style="padding: 20px"><story/></div>',
  }))
  .add('default', () => ({
    components: { SecondaryButton },
    template: `<secondary-button @click="onClick">Click me</secondary-button>`,
    methods,
  }))
  .add('disabled', () => ({
    components: { SecondaryButton },
    template: `<secondary-button @click="onClick" disabled>Disabled button</secondary-button>`,
    methods,
  }))
  .add('block', () => ({
    components: { SecondaryButton },
    template: `<secondary-button @click="onClick" block>Block button</secondary-button>`,
    methods,
  }))
