import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'

import PrimaryButton from './PrimaryButton.vue'

const methods = {
  onClick: action('onButtonClick'),
}

storiesOf('PrimaryButton', module)
  .addDecorator(() => ({
    template: '<div style="padding: 20px"><story/></div>',
  }))
  .add('default', () => ({
    components: { PrimaryButton },
    template: `<primary-button @click="onClick">Click me</primary-button>`,
    methods,
  }))
  .add('disabled', () => ({
    components: { PrimaryButton },
    template: `<primary-button @click="onClick" disabled>Disabled button</primary-button>`,
    methods,
  }))
  .add('block', () => ({
    components: { PrimaryButton },
    template: `<primary-button @click="onClick" block>Block button</primary-button>`,
    methods,
  }))
