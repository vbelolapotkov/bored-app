const budgetRanges = {
  free: [0, 0],
  cheap: [0.01, 0.35],
  middle: [0.35, 0.65],
  expensive: [0.65, 1],
}

export function getBudgetRange(budget) {
  return budgetRanges[budget]
}

export function price2budget(price) {
  for (const budget in budgetRanges) {
    const [min, max] = budgetRanges[budget]
    if (price >= min && price <= max) {
      return budget
    }
  }

  return ''
}
