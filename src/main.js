import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import Icon from './components/Icon'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.component('icon', Icon)

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
